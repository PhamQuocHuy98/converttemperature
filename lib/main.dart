import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Home();
  }
}

class _Home extends State<Home> {
  @override
  final inputC = new TextEditingController();
  final inputF = new TextEditingController();

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Convert temperature"),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 30, right: 30),
        constraints: BoxConstraints.expand(),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 100,
            ),
            TextField(
              onChanged: (text) {
                var value = double.tryParse(text);
                setState(() {
                  inputF.text =
                      ((1.8 * (double.parse(inputC.text))) + 32).toString();
                });
              },
              controller: inputC,
              decoration: InputDecoration(
                hintText: "Enter °C ",
              ),
            ),
            SizedBox(
              height: 30,
            ),
            TextField(
              onChanged: (text) {
                var value = double.tryParse(text);
                setState(() {
                  inputC.text =
                      (((double.parse(inputF.text)) - 32.0) / 1.8).toString();
                });
              },
              controller: inputF,
              decoration: InputDecoration(
                hintText: "Enter °F ",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
